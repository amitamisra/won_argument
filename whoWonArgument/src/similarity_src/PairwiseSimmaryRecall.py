'''
Created on Mar 22, 2015
input=SummaryScusFile,RecallScusFile
Take all the pyramids from summary and recall. 
create a file containing both summary and recall => SummRecallScusFile only for both recall and summary weight > 1
Do a pairwise of labels from of recall and summary,==> SummRecallPairFile, only for recallweight  > 2
Addumbc sim to pairs file
select based on umbc scores top 240

Find how similar they are 
@author: amita, once pairwise 
46 rows recall > 1
37 8376
28 6229_6
33 6229_52
25  5342
'''
import data_pkg.FileHandling
import paraphrase.similarity
import operator
from operator import itemgetter
import itertools
import sys
import copy
import data_pkg.NewFormat_text

# add scus from Allscus json file, only with weight >1
def AddScuType(Allkeys,typeSumm,ScusjsonFile,AllRows, contribtype):
    AllRowsSummaryScusFile=data_pkg.FileHandling.jsontorowdicts(ScusjsonFile)
    if contribtype=="list":
        AllRowsSummaryScusFile= data_pkg.NewFormat_text.changeListtodict(AllRowsSummaryScusFile,"contrib")
    for row in AllRowsSummaryScusFile:
        keyuser=row["key_user"]
        indexuser=str(keyuser).find("user")
        if keyuser[indexuser -1 ] == "_" :
            key=keyuser[: indexuser -1]
        else:
            key= keyuser[:indexuser]   
        if key in Allkeys:
            if int(row["weight"]) > 1 :
                newrow=dict()
                newrow=copy.deepcopy(row)
                newrow["type"]=typeSumm
                del(newrow["key_user"])
                newrow["key"]=key
                AllRows.append(newrow)
        
        
    
# contrib  type for phase 1 , gay marriage was list, change it to dict    
def createrecall_summaryscu(Allkeys,SummaryScusFile,Phase2SummaryScu,RecallScusFile,SummRecallScusFile): 
    AllRows=list()
    typeSumm="summary"
    contribtype="list"
    AddScuType(Allkeys,typeSumm,SummaryScusFile,AllRows,contribtype) 
    contribtype="dir"
    AddScuType(Allkeys,typeSumm,Phase2SummaryScu,AllRows,contribtype) 
    typeSumm="recall"
    AddScuType(Allkeys,typeSumm,RecallScusFile,AllRows,contribtype) 
    fieldnames=AllRows[0].keys()
    data_pkg.FileHandling.writejson(AllRows, SummRecallScusFile)
    data_pkg.FileHandling.write_csv(SummRecallScusFile,AllRows,fieldnames)

def formpairs(SummRecallScusFile,SummRecallPairFile): 
    AllPairRows=[] 
    Rows= data_pkg.FileHandling.read_csv(SummRecallScusFile)
    Rows.sort(key=itemgetter("key"))
    for keyfilename, items in  itertools.groupby(Rows,key=operator.itemgetter("key")):
        groups = []
        types = []
        KeyList=list(items)
        KeyList.sort(key=itemgetter("type"))
        Lists= itertools.groupby(KeyList,key=itemgetter("type"))
        for typesumm, listtype in Lists:
            groups.append(list(listtype))
            types.append(str(typesumm))
        AddPairRow(AllPairRows,groups,types,SummRecallScusFile)  
    fieldnames=sorted(AllPairRows[0].keys())
    AllPairRowssorted=sorted(AllPairRows, key=lambda x: int(operator.itemgetter("recallweight")(x)), reverse= True)
    data_pkg.FileHandling.write_csv(SummRecallPairFile, AllPairRowssorted,fieldnames)
    data_pkg.FileHandling.writejson(AllPairRowssorted, SummRecallPairFile)        
        
def AddPairRow(AllPairRows,groups,types,SummRecallScusFile):
    
    if len(groups) ==2:
        len1=len(groups[0]) # list of summary rows
        len2=len(groups[1])  # list of recall rows
        
        # create a pairwise combination
        for i in range(len1):
            Newrow=dict()
            row1=groups[0][i]
            typesumm=types[0]
            for key1, value1 in row1.iteritems():
                if key1=="key":
                    storekey=value1
                else:    
                    Newrow[typesumm+key1]=value1
            for j in range(len2):
                combinedrow=copy.deepcopy(Newrow)
                row2=groups[1][j]
                typesumm=types[1]
                for key2, value2 in row2.iteritems():
                    if key2=="key":
                        continue
                    combinedrow[typesumm+key2]=value2
                combinedrow["key"]=storekey
                AllPairRows.append(combinedrow)  
    else:
        print "error in groups in function AddPairRow\n"
        print "key" + groups
        sys.exit                
    
    return AllPairRows
def AddUMBC(inputcsv, output,Errorfile ):
    AllRows= data_pkg.FileHandling.read_csv(inputcsv)
    ErrorLines=[]
    AllNewRows=[]
    for row in AllRows:
        NewRow=copy.deepcopy(row)
        s1=row["recalllabel"]
        s2=row["summarylabel"]
        sim= paraphrase.similarity.sss(s1, s2)
        if sim==-1:
            ErrorLines.append("\n\n Error in these sim pairs\n")
            ErrorLines.append("1)"+s1 +"\n")
            ErrorLines.append("2)"+s2+"\n")
        NewRow["UMBC"]=sim
        AllNewRows.append(NewRow)
    fieldnames=AllNewRows[0].keys()    
    data_pkg.FileHandling.write_csv(output,AllNewRows,fieldnames)    
    data_pkg.FileHandling.WriteTextFile(Errorfile,ErrorLines) 
    
      
# limit rows in the pair file to recall weight > 2    
def PairRecallRange(SummRecallPair_UMBC,more2_recallpairsortUMBC,more2_recallpairsortUMBCFields,fieldlist,umbcBeginrow, sampledUMBCPairs)  :
    AllRows= data_pkg.FileHandling.read_csv(SummRecallPair_UMBC)
    more2_recallrows = [ row for row in AllRows if int(row['recallweight']) > 2]
    sortedrows=sorted( more2_recallrows, key = itemgetter('UMBC'))
    lastrow=len(sortedrows)
    samplerowdicts=sortedrows[umbcBeginrow:lastrow]
    newsamplerowdicts=data_pkg.FileHandling.AddUniqueRowNo(samplerowdicts)
    fieldnames=newsamplerowdicts[0].keys()
    data_pkg.FileHandling.writejson(sortedrows,more2_recallpairsortUMBC)
    data_pkg.FileHandling.write_csv(more2_recallpairsortUMBC, sortedrows, fieldnames)
    data_pkg.FileHandling.write_csv(more2_recallpairsortUMBCFields,sortedrows,fieldlist)
    data_pkg.FileHandling.write_csv(sampledUMBCPairs,newsamplerowdicts,fieldnames)
    
      
def Execute(KeyFile,SummaryScusFile,Phase2SummaryScu,RecallScusFile,SummRecallScusFile,SummRecallPairFile,SummRecallPair_UMBC, ErrorFile,more2_recallpairsortUMBC,more2_recallpairsortUMBCFields,fieldlist,umbcrow,sampledUMBCPairs):
    AllRows=data_pkg.FileHandling.read_csv(KeyFile)
    Allkeys=[row["Key"] for row in AllRows  ]
    createrecall_summaryscu(Allkeys,SummaryScusFile,Phase2SummaryScu,RecallScusFile,SummRecallScusFile)
    formpairs(SummRecallScusFile,SummRecallPairFile)
    AddUMBC(SummRecallPairFile, SummRecallPair_UMBC, ErrorFile)
    PairRecallRange(SummRecallPair_UMBC,more2_recallpairsortUMBC,more2_recallpairsortUMBCFields,fieldlist,umbcrow,sampledUMBCPairs)
    
     
    
import os
if __name__ == '__main__':
    KeysFile= os.path.dirname(os.getcwd()) + "/data_pkg_whowon/Wonarg/gay-rights-debates/Map_Keys_Arguments"
    SummaryScusFile="/Users/amita/git/summary_repo/Summary/src/data_pkg/CSV/gay-rights-debates/MTdata/Phase1/LabelUpdated/AllScusJson.json"
    Phase2SummaryScu="/Users/amita/git/summary_repo/Summary/src/data_pkg/CSV/gay-rights-debates/MTdata/Phase2/ComplexAll/Pyramids_Natural/AllScusJson.json"
    
    RecallScusFile="/Users/amita/git/WonArgument/whoWonArgument/src/data_pkg_whowon/Wonarg/gay-rights-debates/RecallPyramid/AllScusJson.json"
    
    SummRecallScusFile= os.path.dirname(os.getcwd()) +  "/data_pkg_whowon/Wonarg/gay-rights-debates/Summ_Recall/more1_SCU_Recall_Summary"
    SummRecallPairFile=os.path.dirname(os.getcwd()) +"/data_pkg_whowon/Wonarg/gay-rights-debates/Summ_Recall/more1_SCU_Pair_Recall_Summary"
    SummRecallPair_UMBC=os.path.dirname(os.getcwd())+"/data_pkg_whowon/Wonarg/gay-rights-debates/Summ_Recall/more1_SCU_Pair_Recall_Summ_UMBC"
    
    more2_recallpairsortUMBC=os.path.dirname(os.getcwd())+ "/data_pkg_whowon/Wonarg/gay-rights-debates/Summ_Recall/more2_recallpairsortUMBC"
    more2_recallpairsortUMBCFields=os.path.dirname(os.getcwd())+ "/data_pkg_whowon/Wonarg/gay-rights-debates/Summ_Recall/more2_recallpairsorUMBC_fields"
    Errorumbc="/Users/amita/git/WonArgument/whoWonArgument/src/data_pkg_whowon/Wonarg/gay-rights-debates/Summ_Recall/ErrorUMBC"
    fieldlist=["UMBC","summaryweight", "recallweight", "summarycontrib",  "recallcontrib" ,"summarylabel", "key","summarytype", "recalllabel"]
    umbcrow=79
    sampledUMBCPairs=os.path.dirname(os.getcwd())+ "/data_pkg_whowon/Wonarg/gay-rights-debates/Summ_Recall/SampledUMBC"
    Execute(KeysFile,SummaryScusFile,Phase2SummaryScu,RecallScusFile, SummRecallScusFile,SummRecallPairFile,SummRecallPair_UMBC,Errorumbc,more2_recallpairsortUMBC,more2_recallpairsortUMBCFields,fieldlist, umbcrow,sampledUMBCPairs)
