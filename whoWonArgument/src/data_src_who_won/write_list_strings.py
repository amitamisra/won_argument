'''
Created on Mar 1, 2015

@author: amita
'''
import csv  
import codecs
def write_csv(outputcsv, rows, fieldnames):
            try:
                        restval=""
                        extrasaction="ignore"
                        dialect="excel"
                        outputfile = codecs.open(outputcsv + ".csv",'w')
                        csv_writer = csv.DictWriter(outputfile, fieldnames, restval, extrasaction, dialect, quoting=csv.QUOTE_NONNUMERIC)
                        csv_writer.writeheader()
                        csv_writer.writerows(rows) 
                        outputfile.close()
            except csv.Error :
                            print "csverror"
                            
            
def read_csv(inputcsv):   
                try:
                    inputfile = codecs.open(inputcsv+ ".csv",'r') 
                    result = list(csv.DictReader(inputfile))
                    return result              
                except csv.Error:
                            print "inputcsverror" 
                            
                            
            
            
            
if __name__ == '__main__':
            rowdict=dict()
            Allrows=list()
            rowdict["stringlist"]=["Abc", "Def"]
            Allrows.append(rowdict)
            fieldnames=["stringlist"]
            outputcsv="teststrings"
            write_csv(outputcsv, Allrows, fieldnames)
            listRows=read_csv(outputcsv)
            print listRows[0]["stringlist"][0]