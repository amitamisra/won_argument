'''
Created on Apr 21, 2015

Calculate the final sim value based on correlation file
@author: amita
'''
from collections import Counter
import os
from data_pkg import FileHandling
from operator import itemgetter
import numpy

# get worker id who meet the minimum criteria for number of hits
def worker_min_pairs(AllRows,fieldname,limit,WorkerPairsCount):
    fieldlist= Counter(tok[fieldname] for tok in AllRows)
    workeridlist=list()
    lines=[]
    for key, values in fieldlist.iteritems():
        lines.append(str(key)+ "   "+ str(values)+"\n")
        if int(values) > 5:
            workeridlist.append(key)
    FileHandling.WriteTextFile(WorkerPairsCount, lines)
    return workeridlist

def calculategoldstandardlabel(highcorr_MinPairslst,AllRows,simlabel):
    count=len(highcorr_MinPairslst)
    for row in AllRows:
        highcorr={}
        stddevlist=[]
        highcorr={your_key: row[your_key] for your_key in highcorr_MinPairslst} 
        totalvalue=0
        for key,value in highcorr.iteritems():
            totalvalue=float(totalvalue)+ float(value)
            stddevlist.append(float(value))
        stddev=standarddev(stddevlist)    
        AvgLabel=float(totalvalue/count)  
        row[simlabel]=AvgLabel
        row["STDDEV"]=stddev
          
    return AllRows       


# calculate standard deviation of a sample, use ddof=1 as sample variance    
def  standarddev(stddevlist):
    arr = numpy.array(stddevlist)
    stddev=numpy.std(arr,ddof=1,dtype=numpy.float64)
    return stddev
    
    
    
#----------------------------- def Append_stddeviation(WorkerPairsCount,stddev):
    #------------------------------------------------------------------ lines=[]
    #-------------------------------------------------------- lines.append("\n")
    #------------------------------------ lines.append(" use final sim label\n")
    #----------------- lines.append("stddeviation _simlabel_finalworkers is:  ")
    #------------------------------------------------- lines.append(str(stddev))
    #---------------------- FileHandling.AppendTextFile(WorkerPairsCount, lines)
    
def Execute(MTResults,MTsplit_workerFile,SimLabels,sortedSimlabel,readablesortedSimlabel,Reducedfieldnames,WorkerPairsCount,highcorr_MinPairslst,fieldname,limit,simlabel):
    AllRows= FileHandling.read_csv(MTResults)
    worker_min_pairs(AllRows,fieldname,limit,WorkerPairsCount)
    AllSplitRows= FileHandling.read_csv(MTsplit_workerFile)
    Rows_label=calculategoldstandardlabel(highcorr_MinPairslst,AllSplitRows,simlabel)
    sortedrows=sorted(Rows_label,key = itemgetter(simlabel), reverse=True)
    fieldnames=Rows_label[0].keys()
    FileHandling.write_csv(SimLabels, Rows_label, fieldnames)
    FileHandling.write_csv(sortedSimlabel ,sortedrows,fieldnames)
    FileHandling.write_csv(readablesortedSimlabel,sortedrows,Reducedfieldnames)
    FileHandling.csvtojson(readablesortedSimlabel)
   
    
        
if __name__ == '__main__':
    topic="gay-rights-debates"    
    inputdir=os.path.dirname(os.getcwd()) +"/data_pkg_whowon/gay-rights-debates/Summ_Recall/"
    MTResults=inputdir +"MT1/MT1Results/Results_MT_Task1"
    MTsplit_workerFile=inputdir +"MT1/MT1Results/MT_Task1_split_worker"
    WorkerPairsCount=inputdir +"/MT1/MT1Results/NO_PairsWorker"
    SimLabels=inputdir +"MT1/MT1Results/SimLabel"
    sortedSimlabel=inputdir +"MT1/MT1Results/SortedSimLabel"
    readablesortedSimlabel=inputdir +"MT1/MT1Results/Sort_Sim_Reduced"
    Reducedfieldnames=sorted(["Id_A1FBBY2JJYRMRI","Id_AJPQEZBVJXVE9","Id_A142ZRU284W9O","summarylabel","summaryweight","recallweight","_uniqueRowNo","key",\
                       "recallcontrib","SimLabel","summarycontrib","recalllabel","STDDEV"])
    
    fieldname="WorkerId"
    limit=5
    simlabel="SimLabel"
    highcorr_MinPairslst=["Id_A1FBBY2JJYRMRI", "Id_A142ZRU284W9O","Id_AJPQEZBVJXVE9"]
    Execute(MTResults,MTsplit_workerFile,SimLabels,sortedSimlabel,readablesortedSimlabel,Reducedfieldnames,WorkerPairsCount,highcorr_MinPairslst,fieldname,limit, simlabel)
    