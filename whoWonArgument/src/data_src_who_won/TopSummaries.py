'''
Created on Jan 16, 2015
get the longest summaries for each dialog
@author: amita
'''
import os 
from nlp.text_obj import TextObj
import nltk
import copy
import re 
from operator import itemgetter
from collections import defaultdict
from data_pkg import FileHandling
import shutil
def word_count(text):
    text_obj= TextObj(text)
    text=text_obj.text
    tokens = text_obj.tokens
    num_words=len(tokens)
    return num_words

# To get the longest summaries sort the input file based  on wordcount for each dialog, create different files for each dialog
def ExtractLongest(FileWithCount, SortedfileDir,num_dialogs ):
    shutil.rmtree(SortedfileDir)
    os.mkdir(SortedfileDir)
    InputRows=FileHandling.read_csv(FileWithCount)
    Fieldnames=sorted(InputRows[0].keys())
    for count in range(1,num_dialogs+1):
        sortvar="arg"+str(count)+"_var4_count"
        sortedRows=sorted(InputRows,key=lambda x: int(x[sortvar]), reverse=True)
        FileName=SortedfileDir+"/won_argum_arg" + str(count)
        FileHandling.write_csv( FileName, sortedRows, Fieldnames)
        
    
# Get the CSv Files from Jeannie and add a word count field
def AddWordCount(InputFile,FileWithCount):
    InputRows=FileHandling.read_csv(InputFile)
    fieldnames=InputRows[0].keys()
    ListRows=list()
    Newfield=list()
    for row in InputRows:
        date_done=row["@end"]
        if "whowon" in str(row["@survey"]):
            monthfinish=date_done.split("/")[0]
            if str(monthfinish).startswith("12"):
                newdict={}
                newdict=copy.deepcopy(row)
                for field in fieldnames:
                    if re.match("arg\d_var4", str(field)):
                        newdict[field+"_count"]=word_count(row[field])
                        Newfield.append(field+"_count") 
                ListRows.append(newdict)
    Newfields=["arg1_var4_count","arg2_var4_count","arg3_var4_count","arg4_var4_count","arg5_var4_count"]            
    Allfields=sorted(fieldnames +  Newfields)
    FileHandling.write_csv(FileWithCount, ListRows, Allfields)        
            
      
    

if __name__ == '__main__':
    topic="gay-rights-debates"
    num_dialogs=5
    InputFile= os.path.dirname(os.getcwd())+ "/data_pkg_whowon/Wonarg/"+ topic+"/whowontheargument_Dec2014"
    FileWithCount=os.path.dirname(os.getcwd())+ "/data_pkg_whowon/Wonarg/"+ topic+"/whowontheargument_Dec2014_Count"
    SortedfileDir=os.path.dirname(os.getcwd())+ "/data_pkg_whowon/Wonarg/"+ topic+"/SortedFiles_count/"
    AddWordCount(InputFile,FileWithCount)
    ExtractLongest(FileWithCount,SortedfileDir,num_dialogs)