'''
Created on Feb 27, 2015
create a csv file with recall summaries, keys, actual summaries.

@author: amita
'''
from data_pkg import FileHandling
from data_pkg import NewFormat_text
import os
import sys
class InputWhowon:
    def __init__(self,recall_summ_dir,mapfile,MTFile,OutFile):
        self.recall_summ_dir=recall_summ_dir
        self.mapfile=mapfile
        self.MTFile=MTFile
        self.OutFile=OutFile
    
    
    def getkey(self,filepath):
        rows=FileHandling.read_csv(self.mapfile)
        keysearch="Argument" + filepath[-5]
        keyrow = [item for item in rows if item["Argument"]== keysearch]
        if (len(keyrow) !=1):
            print " error in keys"
            sys.exit(0)
        else:    
            return keyrow[0]["Key"]
    
    def getMTSummary_dialog(self,key,MT_Summary_list):
        rows= FileHandling.read_csv(self.MTFile)
        if str(key)=="1-5342_11_8__13_16_21_22_1":
            print "stop"
        row=(item for item in rows if item["key"]== key).next()
        summarytext=row["Summary"]
        dialog=row["Dialog"]
        key=row["key"]
        NoregExpDict=FileHandling.removeregularexp(summarytext,MT_Summary_list)
        NoS1Dialog=NewFormat_text.replace_s1s2(dialog)
        return (NoregExpDict,NoS1Dialog,key)
    
    def execute(self):
        AllRows=[]
        filelist=os.listdir(self.recall_summ_dir)
        for filename in filelist:
            row=dict()
            filepath=self.recall_summ_dir+ filename
            key=self.getkey(filepath)
            row["Key_Recall"]=filename
            recalltext=" ".join(FileHandling.ReadTextFile(filepath))
            row["recall_list"]=FileHandling.removeregularexp(recalltext,"recall_list")
            MTTuple=self.getMTSummary_dialog(key,"MT_Summary_list")
            row["MT_Summary_list"]=MTTuple[0]
            row["Dialog"]=MTTuple[1]
            row["key"]=MTTuple[2]
            AllRows.append(row)
        fieldnames=AllRows[0].keys()    
        FileHandling.write_csv(self.OutFile,AllRows, fieldnames)
        FileHandling.writejson(AllRows,self.OutFile)


import os
if __name__ == '__main__':
    topic="gay-rights-debates"
    basedir=os.path.dirname(os.path.dirname(os.getcwd()))
    recall_summ_dir=basedir + "/data/data_whowon/" + topic + "/RecallPyramid/PyramidInput/"
    MapFile=basedir + "/data/data_whowon/" + topic +  "/Map_Keys_Arguments"
    MTFile="/Users/amita/git/summary_repo/Summary/src/data_pkg/CSV/gay-rights-debates/MTdata/AllDialogs_phase_1_2_Formatted"
    OutFile=os.path.dirname(os.path.dirname(os.getcwd())) + "/data/data_whowon/" + topic +"/Recall_MT_Dialog"
    InputWhowonobg =  InputWhowon(recall_summ_dir,MapFile, MTFile,OutFile) 
    InputWhowonobg.execute()