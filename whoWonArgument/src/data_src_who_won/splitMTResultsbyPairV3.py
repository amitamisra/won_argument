'''
Created on Nov 12, 2014
This program takes MT results from MT2 and creates a separate entry for each 
again split the MT results
each row contains a  hit and all the 5 responses from 5 different workers for that hit.


@author: amita
'''
import operator
import itertools
import os
from data_pkg import FileHandling
from collections import defaultdict
from file_formatting import csv_wrapper


def ReadRows(InputCsv):
    rows=FileHandling.read_csv(InputCsv)
    return rows
    
def splitRowPairwise(rowdicts,noofitemsHit,fields):
    AllRows=list()
    rowdicts.sort(key=operator.itemgetter("HITId"))
    for key, items in itertools.groupby(rowdicts, operator.itemgetter('HITId')): # get rows with same HITID
        Hitids=list(items)   # All rows in HItids list have same hitid 
        for count in range(1,noofitemsHit+1):
            NewDict=defaultdict()
            for row in Hitids:
                    NewDict["Id_"+ row["WorkerId"]]=row["Answer.response"+ str(count)]
            for field in fields:
                NewDict[field ]=row["Input."+field+ str(count)]        
            NewDict["HITId"]=row["HITId"]
            NewDict["HITTypeId"]=row["HITTypeId"] 
            AllRows.append(NewDict) 
            
    return AllRows        
                
                    
                    
def Execute(MTinputFile,MTResults,MTsplitResults,noofitemsHit): 
    oneitemrows=ReadRows(MTinputFile)
    fields=oneitemrows[0].keys()
    rowdicts= ReadRows(MTResults)
    NewRows=splitRowPairwise(rowdicts,noofitemsHit,fields)
    csv_wrapper.write_csv(MTsplitResults, NewRows)
    
       
        
        
if __name__ == '__main__':
    topic="gay-rights-debates"    #done for naacl begin end
    inputdir=os.path.dirname(os.getcwd()) +"/data_pkg_whowon/Wonarg/gay-rights-debates/Summ_Recall/"
    MTinputFile=inputdir+"SampledUMBC"  # file used to create MT Task
    MTResults=inputdir +"MT1/MT1Results/Results_MT_Task1"
    MTsplitResults=inputdir +"MT1/MT1Results/MT_Task1_split_worker"
    MTResults2=inputdir +"MT1/MT1Results/redoMT1_A100VVWorker"
    MT2splitResults2=inputdir +"MT1/MT1Results/redoMT1_split_A100VVWorker.csv"
    noofitemsHit=5
    #Execute( MTinputFile,MTResults,MTsplitResults,noofitemsHit)
    Execute( MTinputFile,MTResults2,MT2splitResults2,noofitemsHit)


    #Input=os.getcwd()+ "/"+ topic +"/MTdata_cluster/Labels_Updated/MT_task4/Results/Results_MT_Task4"
    #Output= os.getcwd()+ "/"+ topic+"/MTdata_cluster/Labels_Updated/MT_task4/Results/MT_Task4_split_worker.csv"


    #Input=os.getcwd()+ "/"+ topic +"/MTdata_cluster/Labels_Updated/MT_task2/ResultsV3/MT2_web_interface"
    #Output=os.getcwd()+ "/"+ topic +"/MTdata_cluster/Labels_Updated/MT_task2/ResultsV3/MT2_web_interface_split_worker.csv"
