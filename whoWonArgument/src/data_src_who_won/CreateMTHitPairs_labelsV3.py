'''
Created on Sep 1, 2014
create MT hits for similarity task. combine instances to form a hit file
@author: amita
'''
from data_pkg import FileHandling
import os
from random import shuffle

class MTPairs:
    def __init__(self,Input,Output,No_ofitem,No_ofhits,NotPairEntry):
        self.Input=Input
        self.Output=Output
        self.No_ofitem=No_ofitem
        self.No_ofhits=No_ofhits
        self.fieldnames=[]
        self.NotPairEntry=NotPairEntry
        
    def ReadInput(self):
        Rowdicts=FileHandling.read_csv(self.Input)
        return Rowdicts
        
    def createhitinput(self,rowdicts):
        MTrow=dict()
        #shuffle(rowdicts) 
        Fieldnames=list()
        MTRows=list()
        intc=1
        extc=0
        fields=rowdicts[0].keys()
        for row in rowdicts:
            if extc < self.No_ofhits:  
                if intc<=self.No_ofitem:
                    for field in fields:
                                MTrow[field +str(intc)]=row[field]
                    intc=intc+1       
                                         
                if intc-1==self.No_ofitem:
                    MTRows.append(MTrow)   
                    extc=extc+1
                    intc=1
                    MTrow=dict()
                    
            else:
                break
        #shuffle(MTRows) ( done this once to create MTpairsLabels)   
        Fieldnames=MTRows[0].keys()  
        FileHandling.write_csv(self.Output, MTRows, Fieldnames)            
    
    
def Execute(MTPairsobj):
    RowDicts=MTPairsobj.ReadInput()
    MTPairsobj.createhitinput(RowDicts)
    
def MTRowFilter(MTAllPairs,MT_task1,rowstart,rowend):
    AllRows=FileHandling.read_csv(MTAllPairs)
    fieldnames=sorted(AllRows[0].keys())
    FileHandling.write_csv(MT_task1, AllRows[rowstart:rowend], fieldnames)
    
 
if __name__ == '__main__':
    topic="gun-control"
    inputdir=os.path.dirname(os.getcwd()) +"/data_pkg_whowon/Wonarg/gay-rights-debates/Summ_Recall/"
    inputfile=inputdir+"/SampledUMBC"
    outputfile=inputdir+"MT1/MT1SampledUMBC"
    No_ofitem=5 
    No_ofhits=48
    NotPairEntry=["UMBC"]
    MTPairsobj=MTPairs(inputfile,outputfile,No_ofitem, No_ofhits,NotPairEntry)
    Execute(MTPairsobj)
