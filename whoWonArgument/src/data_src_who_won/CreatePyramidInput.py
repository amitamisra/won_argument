'''
Created on Jan 17, 2015

@author: amita
'''
from data_pkg import FileHandling
import os
from collections import defaultdict
import os
import shutil
def  createummariesfile(SortedFilesdirectory,PyramidDir,Noofsummaries,Noofdialogs):
    filelist=os.listdir(SortedFilesdirectory) 
    dict_key=defaultdict(str)
    for filename in filelist:
        FileWithPath=SortedFilesdirectory + filename[:-4]
        AllRows=FileHandling.read_csv(FileWithPath)
        Top10Rows=AllRows[0:Noofsummaries]
        argumentNo=FileWithPath[-1]
        for count in range(0,Noofsummaries):
            row=Top10Rows[count]
            Text=row["arg" + str(argumentNo)+"_var4"]
            dict_key[filename[:-4]]=dict_key[filename[:-4]]+"\n"+ str(RegularExp[count]) + "\n" + Text +"\n\n"
        #shuffle(Allsummary_list)
        #FileHandling.write_csv(summaryFile, Allsummary_list, fieldnames)
        
        
    for key in dict_key.keys():
        Outfile=PyramidDir + "/"+key
        Lines=list()
        Lines.append(dict_key[key])
        FileHandling.WriteTextFile(Outfile, Lines)
if __name__ == '__main__':
    Noofsummaries=10
    Noofdialogs=5
    topic="gay-rights-debates"
    RegularExp=["----------\nD"+ str(i) +"\n"+ "----------" for i in range(Noofsummaries)]  
    SortedFilesdirectory=os.path.dirname(os.getcwd())+ "/data_pkg_whowon/Wonarg/"+ topic+"/SortedFiles_count/"
    PyramidDir=os.path.dirname(os.getcwd())+ "/data_pkg_whowon/Wonarg/"+ topic+"/RecallPyramid/PyramidInput"
    shutil.rmtree(PyramidDir)
    os.mkdir(PyramidDir)
    filename="won_argum_arg"
    createummariesfile(SortedFilesdirectory,PyramidDir,Noofsummaries,Noofdialogs)
    