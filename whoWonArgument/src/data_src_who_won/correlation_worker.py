'''
Created on Apr 20, 2015
call an R Script to calculate worker correlations for MT task
Takes input as  MTsplitResults as MT1Results/MT_Task1_split_worker.csv"
@author: amita
'''
from data_pkg import FileHandling
import os

# select column for correlation
def createMTworker_UMBCcolumn(MTsplitResults):
    AllRows=FileHandling.read_csv(MTsplitResults)
    fields=AllRows[0].keys()
    columnlist=[column for column in fields if str(column).startswith("Id_")]
    columnlist=columnlist + ["UMBC"]
    return columnlist

#create input file for calculating correlation coeff
def createcorrelationinput(columnlist,MTsplitResults, corrinputfile):
    AllRows=FileHandling.read_csv(MTsplitResults)
    FileHandling.write_csv(corrinputfile,AllRows,columnlist)
    
def calculatecorr(corrinputfile,corroutputfile):  
    print
    # do actual script in R
      
    
def Execute(MTsplitResults,corrinputfile,corroutfile,MTworker_UMBCcolumnlist):
    MTworker_UMBCcolumnlist= createMTworker_UMBCcolumn(MTsplitResults)  
    createcorrelationinput(MTworker_UMBCcolumnlist,MTsplitResults, corrinputfile)
      
if __name__ == '__main__':
    inputdir=os.path.dirname(os.getcwd()) +"/data_pkg_whowon/Wonarg/gay-rights-debates/Summ_Recall/"
    MTsplitResults=inputdir +"MT1/MT1Results/MT_Task1_split_worker"
    corrinputfile=inputdir +"MT1/MT1Results/MT_Task1_split_worker_corrcolumn"
    corroutfile=inputdir +"MT1/MT1Results/MT_Task1_split_worker_corr"
    MTworker_UMBCcolumnlist=[]
    #Execute(MTsplitResults,corrinputfile,corroutfile,MTworker_UMBCcolumnlist) 
    
    corrinputfile=inputdir +"MT1/MT1Results/MT1Final_split_worker_corrcolumn"
    corroutfile=inputdir +"MT1/MT1Results/MT1Final_split_worker_corr"
    MTFinal_split_worker= inputdir +"MT1/MT1Results/MTFinal_split_worker"
    Execute(MTFinal_split_worker,corrinputfile,corroutfile,MTworker_UMBCcolumnlist)