#encoding: utf-8
'''
Created on Sep 14, 2014
this program takes input as input strings/summaries/dialogs from csv files , creates a corresponding file for the singapore parser
.singapore parser reads these files, writes to the output files and creates a corresponding json file for all the strings/summaries/dialogs 
@author: amita
'''
import os
import FileHandling
import subprocess
import sys
class SingaporeParser:
    def __init__(self,Inputfile,ParserPath,Relationdir):
        self.Inputfile=Inputfile
        self.ParserPath=ParserPath
        self.RelationDir=Relationdir
    
    
    #===========================================================================
    # This function takes an input a field from /Users/amita/git/WonArgument/whoWonArgument/src/Data_Pkg/data_pkg_whowon/Wonarg/gay-rights-debates/Recall_MT_Dialog.csv
    # and writes each string to a file for singapore parser
    #===========================================================================
    def write_strings_File(self,field):
        RowStrings=FileHandling.read_csv(self.Inputfile)
        counter=1
        for row in RowStrings:
            Lines=list()
            InputKey=row["Key"]
            SummaryList=row[field]
            if not os.path.exists(self.RelationDir):
                os.mkdir(self.RelationDir)
            if not os.path.exists(self.RelationDir +"/"+ InputKey):
                os.mkdir(self.RelationDir+"/"+ InputKey)
            if not os.path.exists(self.RelationDir+ "/"+ InputKey+"/"+field):
                os.mkdir(self.RelationDir+ "/"+ InputKey+"/"+field)
            for fielddata in SummaryList:     
                OutStringFileParser = self.RelationDir +"/"+ InputKey +"/"+ field + "/"+ field + "_" + str(counter)
                Lines.append(fielddata)
                counter=counter+1
                FileHandling.WriteTextFile(OutStringFileParser, Lines)
     
    def discourseParser(self,parserlocation,Filewithpath):
        cmd=str(parserlocation +" "+ Filewithpath) 
        proc=subprocess.Popen(cmd, shell=True, stdout=subprocess.PIPE) 
        output, errors=proc.communicate()
        return output
              
            
    def write_dicourse_strings(self):
        try: 
            AllRows=list()  
            Relation_Lines=list()    
            ListDir=os.listdir(self.RelationDir)
            for KeyDirs in ListDir: 
                if str(KeyDirs).startswith("."):
                    continue
                SummFileList=os.listdir(self.RelationDir+"/"+ KeyDirs)
                summ_count=1
                for filename in SummFileList:
                    if str(filename).startswith("."):
                        continue
                    FileWithPath=self.RelationDir+"/"+ KeyDirs+"/"+ SummFileList+"/"+ filename
                    TextLines=FileHandling.ReadTextFile(FileWithPath)
                    print FileWithPath
                    output=self.discourseParser(self.ParserPath,FileWithPath)
                    TextString=" ".join(TextLines)
                    Rowdict=dict()
                    #subprocess.call(cmd, shell=True) 
                    #output=subprocess.check_output(cmd,shell=True) 
                    Rowdict["key"]  = KeyDirs
                    Rowdict["Sent"]  = TextString
                    Rowdict["Parsed_String"]  = output
                    Rowdict["Summ_No"]=summ_count
                    summ_count=summ_count+1;
                    AllRows.append(Rowdict)
                    Relation_Lines.append(output)
                    
            FileHandling.WriteTextFile(self.RelationFile,Relation_Lines)  
            FileHandling.write_csv(self.RelationFile, AllRows, self.fieldnames)                                                                       
        except Exception as e :
            print " Exception " + str(e)  +"in File SingaporeParser in function write_dicourse_stringsFile"  
            sys.exit()  #proc=subprocess.Popen(cmd, shell=True, stdout=subprocess.PIPE, stderr=subprocess.STDOUT)                              
                #for line in proc.stdout:
                #    print line
                #subprocess.call([self.ParserPath,OutFileDes], shell=True)
                #output, errors = p.communicate()  
                #OutFileDes.close()
                
        
def Execute(SingaporeParserObj):
    fields=["MT_Summary_list","Dialog","recall_list"]
    for field in fields:
        SingaporeParserObj.write_strings_File(field)
        SingaporeParserObj.write_dicourse_stringsFile(field)
                

if __name__ == '__main__':
    topic="gay-rights-debates"
    parserlocation="/Users/amita/software/singapore_pdtb-parser-master/src/parse.rb"
    InpFileLocation=os.path.dirname(os.getcwd())+"/data_pkg_whowon/Wonarg/" + topic +"/Recall_MT_Dialog"
    RelationDir=os.path.dirname(os.getcwd())+"/data_pkg_whowon/Wonarg/" + topic +"/DiscourseParsedRelations" # Allrelations in all strings as textfile
    # TextDir=os.path.dirname(os.getcwd())+"/data_pkg_whowon/Wonarg/" + topic + "/Parserdata/"  # contains each string as a file for parser input
    SingaporeParserObj=SingaporeParser(InpFileLocation,parserlocation,RelationDir)
    Execute(SingaporeParserObj)
            
    
    
    
    
    
    